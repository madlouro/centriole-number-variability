import numpy as np
from sympy import *
from sympy.utilities.lambdify import lambdastr
from scipy.optimize import minimize
import csv
import os
import time

def log_likelihood(obs,exp,pars,i_max):

    'Function for writing the log-likelihood function for a model'

    ll_exp = 0 #start a log-likelihood counter
    obs_rem_null=[elem for elem in obs if elem!=""] #list of centriole numbers per cell
    obs_arr=np.array(sympify(obs_rem_null))-4 #subtract 4 centrioles to each cell (thus, the data are the number of extra centrioles)
    obs_counts=np.bincount(list(obs_arr),minlength=i_max+1) #bin centriole numbers per cell into categories
    
    for i in range(len(obs_counts)): #for all centriole number categories

        ll_exp += -obs_counts[i]*log(sympify(exp[i])) #write log-likelihood expression

    return lambdify(pars,ll_exp) #create a lambda function

def fit_model(data,exp,init,pars,i_max,bnds,cons,out_file):

    'Function for fitting a model to all the empirical distributions'

    output=open(out_file,"w") #create output file
    writer=csv.writer(output) #create csv writer object

    for d in data: #for each empirical distribution

        ll=log_likelihood(d,eq_v,pars,i_max) #write log-likelihood function

        def ll_p(pars): #definition for parametrizing lambda function

            return ll(*tuple(pars))

        max_ll=minimize(ll_p,init,method='trust-constr',bounds=bnds,constraints=cons) #maximize the log-likelihood function (literally, minimize the negative of the log-likelihood function)
        est=[str(val) for val in max_ll.x] #estimated parameter values (in string
        writer.writerow([str(-max_ll.fun)]+est) #write maximum log-likelihood value and estimated parameter values
        print(-max_ll.fun) #print maximum log-likelihood value
        
    output.close() #close output file

    return None

models=('EqMR1.csv','EqMR12.csv','EqMR14.csv','EqMR124.csv','EqMC1.csv','EqMC12.csv','EqMC14.csv','EqMC124.csv','EqMLambda1.csv','EqMLambda12.csv','EqMLambda14.csv','EqMLambda124.csv') #equilibrium expressions for the 12 models
r,c,lamb,mu1,mu2,mu4=(Symbol('r'),Symbol('c'),Symbol('\u03bb'),Symbol('\u03bc'+'1'),Symbol('\u03bc'+'2'),Symbol('\u03bc'+'4')) #model parameters
all_pars=((r,mu1),(r,mu1,mu2),(r,mu1,mu4),(r,mu1,mu2,mu4),(c,mu1),(c,mu1,mu2),(c,mu1,mu4),(c,mu1,mu2,mu4),(lamb,mu1),(lamb,mu1,mu2),(lamb,mu1,mu4),(lamb,mu1,mu2,mu4)) #parameter sets for the 12 models
out_files=('MR1_fit_py.csv','MR12_fit_py.csv','MR14_fit_py.csv','MR124_fit_py.csv','MC1_fit_py.csv','MC12_fit_py.csv','MC14_fit_py.csv','MC124_fit_py.csv','MLambda1_fit_py.csv','MLambda12_fit_py.csv','MLambda14_fit_py.csv','MLambda124_fit_py.csv') #output file names
bnds=(((-1.,1.),(0.,1.)),((-1.,1.),(0.,1.),(0.,1.)),((-1.,1.),(0.,1.),(0.,1.)),((-1.,1.),(0.,1.),(0.,1.),(0.,1.)),((0.,2.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.),(0,1.)),((0.,2.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.)),((0.,2.),(0.,1.),(0.,1.),(0,1.))) #lower and upper bounds for each parameter, for each of the parameter sets
init=((0,0.01),(0,0.01,0.01),(0,0.01,0.01),(-0.5,0.01,0.01,0.01),(1.5,0.5),(1.5,0.5,0.2),(1.5,0.5,0.2),(1.8,0.5,0.2,0.2),(0.,0.01),(0,0.01,0.01),(0.,0.01,0.01),(0.2,0.5,0.5,0.5)) #initial parameter values (seed for the minimization algorithm)

i_max=30 #maximum number of centrioles per cell

for m in range(len(models)): #for each model

    start=time.time() #store start time
    data_path='/Users/madlouro/AllDataFiltered.csv' #path of the data file (empirical distributions of centriole numbers per cell for each cell line)
    data_file=open(data_path,'r') #open data file
    data=csv.reader(data_file) #generate reader object

    eq_path='/Users/madlouro/'+models[m] #path for each equilibrium expression file
    eq_file=open(eq_path,'r') #open equilibrium expression file
    eq_reader=csv.reader(eq_file) #generate reader object
    eq_v=list(np.concatenate([sympify(eq) for eq in eq_reader])) #convert each equilibrium expression to a simpy object

    pars=all_pars[m] #parameter set of model m
    if m<=3:

        cons = [{'type': 'ineq', 'fun': lambda pars:  -sum(pars)+1}] #constraints for models with flat fitness functions

    elif m>3 and m<=7:

        cons = [{'type': 'ineq', 'fun': lambda pars:  -sum(pars[1::])+pars[0]}] #constraints for models with linear fitness functions
        
    else:

        cons = [{'type': 'ineq', 'fun': lambda pars:  -sum(pars[1::])+2}] #constraints for models with power law fitness functions
        
    fit_model(data,eq_v,init[m],pars,i_max,bnds[m],cons,out_files[m]) #fit model to all empirical distributions

    data_file.close() #close data file
    
    print(models[m]) #print model m

    end=time.time() #store final time
    print(end-start) #print duration of each iteration
