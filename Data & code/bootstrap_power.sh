#!/bin/bash
bootstrap_list="$1"
IFS=', ' read -r -a bootstrap_array <<< $bootstrap_list
model_list="$2"
IFS=', ' read -r -a model_array <<< $model_list
eq_file_list="$3"
IFS=', ' read -r -a eq_array <<< $eq_file_list
imax_exp=$4
s1=$5
s2=$6
outfile_list="$7"
IFS=', ' read -r -a outfile_array <<< $outfile_list
pars=$8
const=$9

#models=$(cat $6 | wc -l)

for m in $(seq 1 1 3)
do
	eq=${eq_array[$(( m - 1 ))]}
	model=${model_array[$(( m - 1 ))]}
	for f in $(seq 1 1 8)
	do
		bootstrap_data=${bootstrap_array[$(( f - 1 ))]}
#		bootstrap_samples=$(awk -v d="1" 'NR==d' "$bootstrap_data")
		outfile=${outfile_array[$(( f - 1 ))]}
		outfile="${model}_${outfile}"
		for b in $(seq $s1 1 $s2)
		do
			/Applications/Mathematica11.app/Contents/MacOS/wolframscript -file /Applications/bootstrap.wls 1 $b $bootstrap_data $m $eq $imax_exp $outfile $pars $const
			echo $m $f $b
		done
	done
done
