#!/bin/bash
cline=$(cat $1 | wc -l) #number of cell lines
eq_file_list="$3" #string of CSV files containing steady-state expressions (including the extension)
IFS=', ' read -r -a eq_array <<< $eq_file_list #convert string of files containing steady-state expressions to an array
m="$4" #model for fitting
eq=${eq_array[$(( m - 1 ))]} #steady-state expression corresponding to the model
imax_exp=$5 #maximum number of centrioles per cell
s1=$6 #initial bootstrap distribution
s2=$7 #final bootstrap distribution
outfile_list="$8" #string of output file names
IFS=', ' read -r -a outfile_array <<< $outfile_list #convert string of output file names to an array
outfile=${outfile_array[$(( m - 1 ))]} #outfile name corresponding to the model
pars=$9 #string of the CSV file containing model parameters
const=${10} #string of the CSV file containing model constraints

for d in $(seq 1 1 $cline) #for each cell line
do
	bootstrap_samples=$(awk -v d="$d" 'NR==d' "$2") #import corresponding bootstrap distributions
	for b in $(seq $s1 1 $s2) #for each bootstrap distribution
	do
		/Applications/Mathematica11.app/Contents/MacOS/wolframscript -file /Applications/fit_model_bootstrap.wls $d $b "${bootstrap_samples}" $m $eq $imax_exp $outfile $pars $const #fit model
		echo $d $b #print cell line and bootstrap distribution indices
	done
done

#example of script call

#/Applications/fit_bootstrap.sh AllDataFiltered.csv bootstrapSamplesNonParametric1.csv 'EqF1.mx EqF12.mx EqF14.mx EqF124.mx EqL1.mx EqL12.mx EqL14.mx EqL124.mx EqE1.mx EqE12.mx EqMLambda14.mx EqE124.mx EqF124Alt.mx' 1 30 1 100 F1p1 F12p1 F14p1 F124p1 L1p1 L12p1 L14p1 L124p1 E1p1 E12p1 E14p1 E124p1 F124Altp1' params.mx constraints.mx
